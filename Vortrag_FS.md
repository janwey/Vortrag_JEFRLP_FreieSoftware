---
title: Kann Freie, Open Source Software helfen Demokratie und Wirtschaft in Europa zu stärken?
description: Online-Stammtisch der JEF RLP, 10.06.2020
---

<!-- class: invert -->

![bg](./img/bg_00.jpg)

# Kann Freie, Open Source Software helfen Demokratie und Wirtschaft in Europa zu stärken?

## Online-Stammtisch der JEF RLP, 10.06.2020

> Einführung von: Jan Weymeirsch
> &#128231; [weymeirsch![at](./img/at.svg)jef-rlp.de](mailto:&#119;&#101;&#121;&#109;&#101;&#105;&#114;&#115;&#099;&#104;[&#065;&#084;]&#106;&#101;&#102;&#045;&#114;&#108;&#112;&#046;&#100;&#101;)
> &#128421; [jef-rlp.de/ueber-uns/#weymeirsch](https://www.jef-rlp.de/ueber-uns/#weymeirsch)
> &#9852; [codeberg.org/janwey/Vortrag_JEFRLP_FreieSoftware](https://codeberg.org/janwey/Vortrag_JEFRLP_FreieSoftware)

![Junge Europäische Föderalisten Rheinland-Pfalz](./img/jefrlp.png)

----

![bg](./img/bg_02.jpg)

## Was ist "Freie Software"?

Die [Free Software Foundation Europe](https://fsfe.org) definiert Software als "Freie Software", wenn sie folgende vier Freiheiten erfüllt:

![4 Freiheiten Freier Software](./img/4freedoms.png)
> (Siehe auch: https://fsfe.org/freesoftware/freesoftware.de.html)

----

![bg](./img/bg_03.jpg)

## Chancengleichheit in der Wirtschaft

### Proprietäre Software:

- Software und Dienste haben die Tendenz zur **Monopolbildung** und somit **Einfluss auf Wettbewerb**, **Unternehmen** und **Nutzende**.
- Änderungen sind an **Wirtschaftlichkeit und Abwägung der Entwickler\*innen** geknüpft.

### Freie Software:

- Schafft **echten Wettbewerb**: Nutzende können transparent zwischen den Alternativen abwägen.
- Nutzende können **Änderungen selbst vornehmen** oder Dritte dafür beauftragen (Zusätzliche Sprachausgaben, Nutzungsbarrieren für Blinde, etc)

----

![bg](./img/bg_04.jpg)

## Chancengleichheit in der Gesellschaft

Technologie ist ein **zentraler Bestandteil moderner & globaler Kommunikation** in einer vernetzten Welt. Gerade neue Technologien wie KI weisen oftmals Probleme für Bevölkerungsgruppen auf, welche nicht der **sozioökonomischen Gruppe der Entwickler\*innen** entsprechen.

**&#10071; Benötigte Lösungen sind langwierig und kompliziert.**

Freie Software kann eine Teillösung sein:

- &#9989; verringert **Einstiegs- & Nutzungsbarrieren**
- &#9989; ermöglicht die **direkte und indirekte Einflussnahme** von Nutzenden

----

![bg](./img/bg_05.jpg)

## Einfluss von Software auf Menschenrechte

Kommunikation und Partizipation an Gesellschaft sind heute oftmals an Technologie geknüpft. Softwareanbietenden wird somit implizit große Macht über z.B. den öffentlichen Diskurs eingestanden.

Freie Software ist kein Garant für uneingeschränkte Kommunikation aber eine unabdingbare Voraussetzung.

----

![bg](./img/bg_06.jpg)

## Freie Software schafft Vertrauen

Da der **Quellcode von Freier Software für Alle einsehbar** ist, können Nutzende unliebsame Aspekte einfach entfernen (lassen).

**Fehler und Sicherheitslücken** können durch "Public Audit" schnell gefunden und geschlossen werden.

Die **Nutzung Freier Software ist langfristig planbar**, selbst dann wenn die ursprünglichen Autor\*innen diese nicht mehr entwickeln.

----

![bg](./img/bg_07.jpg)

## Freie Software sichert Demokratie

In Demokratien **verteilen wir Macht auf sich gegenseitig kontrollierende Sektoren**. Im föderalen System wird Macht auf verschiedene Ebenen verteilt.

&#10140; Selbst wenn Feinde der Demokratie an Schlüsselstellen sitzen, soll das demokratische System wehrhaft bleiben.

Das **Entwickeln und Bereitstellen von Technologie erzeugt auch Macht**, wenn Softwarehaltende über die Nutzung und Verbreitung bestimmen können. Vergangenheit zeigt: Regierungen **nutzen Monopolstellung** von bei ihnen ansässigen Softwareunternehmen um Druck auf Andere auszuüben.

&#9989; Nur Freie Software alleine ist in der Lage **exekutive Prozesse von Regierungen und Verwaltungen transparent und nachprüfbar abzubilden**.

----

![bg](./img/bg_08.jpg)

## Was hat das mit dir zu tun?

Verbreitete These: *"Die Entscheidung zur Nutzung von unfreier Software ist ein rein persönliches Anliegen, welches keinen Effekt auf Andere hat."*

**&#10071; Aber:**
Wird proprietäre Software z.B. zur Kollaboration oder Kommunikation genutzt, so werden auch Andere zur Nutzung gedrängt/gezwungen. Aus einer **individuellen, freien Entscheidung** wird eine **Missachtung der Selbstbestimmung Anderer** (Siehe hierzu auch "Lock-In Effekt").

----

![bg](./img/bg_01.jpg)

# Antrag \#1

Der Bundeskongress möge beschließen, dass
- in den internen und externen Abläufen der Jungen Europäischen Föderalisten Deutschland Freie Software bisher genutzte proprietäre Softwarelösungen mittelfristig überall dort ersetzt, wo dies möglich ist.
- bei Neuanschaffung von Softwarelösungen und Services Freie Software präferiert wird.
- Landes- und Kreisverbände der Jungen Europäischen Föderalisten Deutschland auch zur Nutzung Freier Software angehalten werden.

&#11208; ["Antrag zu Freier Software" auf cloud.jef-rlp.de](https://nextcloud03.webo.hosting/s/5AXtBGyxPF2moxD)

----

![bg](./img/bg_01.jpg)

# Antrag \#2

Der Bundeskongress möge beschließen, dass
- sich die Jungen Europäischen Föderalisten für das Nutzen von Freier Software in öffentlichen Verwaltungen und Institutionen aussprechen.
- sich die Jungen Europäischen Föderalisten als "unterstützende Organisation" der "Public Money? Public Code!" Initiative auf [publiccode.eu/#organisations](https://publiccode.eu/de/#organisations) eintragen lassen.

&#11208; ["Antrag zu 'Public Money? Public Code!'" auf cloud.jef-rlp.de](https://nextcloud03.webo.hosting/s/X6EkqopfGCdegoJ)

