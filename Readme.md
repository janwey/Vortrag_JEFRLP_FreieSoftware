Context: https://www.jef-rlp.de/2020/06/08/online-stammtisch-freie-software

Final Presentation slides: https://codeberg.org/janwey/Vortrag_JEFRLP_FreieSoftware/raw/branch/master/Vortrag_FS.pdf

Uses assets from: https://git.fsfe.org/pmpc/website/

Uses [Marp](https://marp.app/) for compiling:

```
# for HTML output
make html

# for PDF output
make pdf
```
